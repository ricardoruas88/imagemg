package br.com.softwarecwb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

@Configuration
@ComponentScan("br.com.softwarecwb")
@EnableAutoConfiguration
public class ImagemGApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ImagemGApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(ImagemGApplication.class, args);
	}

	// @Bean
	// public MultipartResolver multipartResolver() {
	// CommonsMultipartResolver multipartResolver = new
	// CommonsMultipartResolver();
	// multipartResolver.setMaxUploadSize(10000000);// 100k
	// return multipartResolver;
	// }

	@Bean
	public MultipartResolver multipartResolver() {

		return new StandardServletMultipartResolver();
	}
}
