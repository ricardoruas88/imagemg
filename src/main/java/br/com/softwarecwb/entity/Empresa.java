package br.com.softwarecwb.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "empresas")
public class Empresa extends AbstractPersistable<Integer> {

	private static final long serialVersionUID = 1L;

	private String nome;
	
	@JsonProperty("logomarca")
	@Lob
	@Column(nullable = true)
	private byte[] logomarca;
	
	@JsonIgnore
	@OneToMany(mappedBy = "empresa", targetEntity = Produto.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Produto> produtos = new ArrayList<>();

	public Empresa() {
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void adiciona(Produto produto) {
		this.produtos.add(produto);
		produto.setEmpresa(this); // mantém a consistência
	}

	public List<Produto> getProdutos() {
		List<Produto> listaSegura = Collections.unmodifiableList(this.produtos);
		return listaSegura;
	}

	public byte[] getLogomarca() {
		return logomarca;
	}

	public void setLogomarca(byte[] logomarca) {
		this.logomarca = logomarca;
	}
}
