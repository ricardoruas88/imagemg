package br.com.softwarecwb.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;

import br.com.softwarecwb.service.xml.ItemXML;

@Entity
@Table(name = "produtos")
public class Produto extends AbstractPersistable<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(nullable = false, length = 36)
	private String codigo;

	@Column(nullable = false, length = 255)
	private String nome;

	@Column(nullable = false, length = 255)
	private String link;

	@Column(nullable = true, length = 500)
	private String descricao;

	@ManyToOne
	@JoinColumn(name="empresa_id")
	private Empresa empresa;

	@Lob
	@Column(nullable = true)
	private byte[] imgQrCode;

	public Produto() {
	}
	
	public Produto(ItemXML item) {
		this.codigo = item.getCodigo();
		this.nome = item.getNome();
		this.link = item.getLink();
		this.descricao = item.getDescricao();
	}

	public Produto(String codigo, String nome, String link, String descricao, Empresa empresa, byte[] imgQrCode) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.link = link;
		this.descricao = descricao;
		this.empresa = empresa;
		this.imgQrCode = imgQrCode;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public byte[] getImgQrCode() {
		return imgQrCode;
	}

	public void setImgQrCode(byte[] imgQrCode) {
		this.imgQrCode = imgQrCode;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	
}
