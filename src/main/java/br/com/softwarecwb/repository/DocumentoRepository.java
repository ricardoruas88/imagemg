package br.com.softwarecwb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.softwarecwb.entity.Documento;

public interface DocumentoRepository extends JpaRepository<Documento, Integer> {

}
