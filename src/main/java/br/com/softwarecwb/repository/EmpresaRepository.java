package br.com.softwarecwb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.softwarecwb.entity.Empresa;

public interface EmpresaRepository extends JpaRepository<Empresa, Integer> {

}
