package br.com.softwarecwb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.softwarecwb.entity.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Integer>{
}
