package br.com.softwarecwb.restcontroller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import javax.servlet.annotation.MultipartConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.softwarecwb.entity.Documento;
import br.com.softwarecwb.restcontroller.generic.AbstractController;
import br.com.softwarecwb.service.DocumentoService;

@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB
		maxFileSize = 1024 * 1024 * 50, // 50 MB
		maxRequestSize = 1024 * 1024 * 100)
@RestController
@RequestMapping("/documento")
public class DocumentoController extends AbstractController<Documento, Integer> {

	@Autowired
	public DocumentoController(DocumentoService service) {
		super(service);
	}

	@RequestMapping(value = "uploadFile", method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestParam(value = "documento") String documento,
			@RequestParam("file") MultipartFile file) {
		try {
			Documento documentos = new ObjectMapper().readValue(documento, Documento.class);

			if (!file.isEmpty()) {

				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File(file.getOriginalFilename())));
				stream.write(bytes);
				stream.close();
				documentos.setDocumento(bytes);
				documentos.setFilename(file.getOriginalFilename());
			}

			service.save(documentos);

			return new ResponseEntity<Void>(HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/download/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> downloadFile(@PathVariable("id") Integer id) {

		Documento fileUpload = service.findById(id);

		// No file found based on the supplied filename
		if (fileUpload == null) {
			return new ResponseEntity<>("{}", HttpStatus.NOT_FOUND);
		}

		// Generate the http headers with the file properties
		HttpHeaders headers = new HttpHeaders();
		headers.add("content-disposition", "attachment; filename=" + fileUpload.getFilename());

		// Split the mimeType into primary and sub types
		String primaryType, subType;
		try {
			primaryType = fileUpload.getTipo().split("/")[0];
			subType = fileUpload.getTipo().split("/")[1];
		} catch (IndexOutOfBoundsException | NullPointerException ex) {
			return new ResponseEntity<>("{}", HttpStatus.INTERNAL_SERVER_ERROR);
		}

		headers.setContentType(new MediaType(primaryType, subType));

		return new ResponseEntity<>(fileUpload.getDocumento(), headers, HttpStatus.OK);
	}
}