package br.com.softwarecwb.restcontroller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import javax.servlet.annotation.MultipartConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.softwarecwb.entity.Empresa;
import br.com.softwarecwb.restcontroller.generic.AbstractController;
import br.com.softwarecwb.service.EmpresaService;

@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB
		maxFileSize = 1024 * 1024 * 50, // 50 MB
		maxRequestSize = 1024 * 1024 * 100)
@RestController
@RequestMapping("/empresa")
public class EmpresaController extends AbstractController<Empresa, Integer> {

	@Autowired
	public EmpresaController(EmpresaService service) {
		super(service);
		// TODO Auto-generated constructor stub
	}

	@RequestMapping(value = "uploadFile", method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestParam(value = "empresa") String empresa,
			@RequestParam("file") MultipartFile file) {
		try {
			Empresa empresas = new ObjectMapper().readValue(empresa, Empresa.class);

			if (!file.isEmpty()) {

				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File(file.getName())));
				stream.write(bytes);
				stream.close();
				empresas.setLogomarca(bytes);
			}

			service.save(empresas);

			return new ResponseEntity<Void>(HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		}
	}
}
