package br.com.softwarecwb.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.softwarecwb.entity.Documento;
import br.com.softwarecwb.entity.Produto;
import br.com.softwarecwb.restcontroller.generic.AbstractController;
import br.com.softwarecwb.service.ProdutoService;
import br.com.softwarecwb.service.util.QrCodeUtils;

@RestController
@RequestMapping("/produto")
public class ProdutoController extends AbstractController<Produto, Integer> {

	@Autowired
	public ProdutoController(ProdutoService service) {
		super(service);
	}
	
	
	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Produto> update(@PathVariable("id") Integer id, @RequestBody Produto produto) {

		Produto currentProduto = service.findById(id);

		if (currentProduto == null) {
			return new ResponseEntity<Produto>(HttpStatus.NOT_FOUND);
		}

		currentProduto.setNome(produto.getNome());
		currentProduto.setDescricao(produto.getDescricao());
		currentProduto.setLink(produto.getLink());
		currentProduto.setEmpresa(produto.getEmpresa());
		currentProduto.setImgQrCode(QrCodeUtils.generateQrCode(produto.getLink()));

		service.update(currentProduto);
		return new ResponseEntity<Produto>(currentProduto, HttpStatus.OK);
	}
	@Override
	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody Produto produto) {
		produto.setImgQrCode(QrCodeUtils.generateQrCode(produto.getLink()));
		service.save(produto);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/download/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> downloadFile(@PathVariable("id") Integer id) {

		Produto fileUpload = service.findById(id);

		// No file found based on the supplied filename
		if (fileUpload == null) {
			return new ResponseEntity<>("{}", HttpStatus.NOT_FOUND);
		}

		// Generate the http headers with the file properties
		HttpHeaders headers = new HttpHeaders();
		headers.add("content-disposition", "attachment; filename=" + fileUpload.getNome());
		
		headers.setContentType(new MediaType("image","png"));

		return new ResponseEntity<>(fileUpload.getImgQrCode(), headers, HttpStatus.OK);
	}
}
