package br.com.softwarecwb.restcontroller.generic;

import java.io.Serializable;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.softwarecwb.service.generic.AbstractService;


public abstract class AbstractController<T, ID extends Serializable> {

	protected AbstractService<T, ID> service;

	public AbstractController(AbstractService<T, ID> service) {
		this.service = service;
	}
	// -------------------Retrieve All
	// T--------------------------------------------------------

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<List<T>> listAll() {
		List<T> ts = service.findAll();
		if (ts.isEmpty()) {
			return new ResponseEntity<List<T>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<T>>(ts, HttpStatus.OK);
	}
	// -------------------Retrieve Single
	// T--------------------------------------------------------

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<T> get(@PathVariable("id") ID id) {
		T t = service.findById(id);
		if (t == null) {
			return new ResponseEntity<T>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<T>(t, HttpStatus.OK);
	}

	// -------------------Create a
	// T--------------------------------------------------------

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody T t) {
		service.save(t);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	// ------------------- Update a T
	// --------------------------------------------------------

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<T> update(@PathVariable("id") ID id, @RequestBody T t) {
		T currentT = (service.findById(id));

		if (currentT == null) {
			return new ResponseEntity<T>(HttpStatus.NOT_FOUND);
		}
		service.update(currentT);
		return new ResponseEntity<T>(currentT, HttpStatus.OK);
	}

	// ------------------- Delete a T
	// --------------------------------------------------------

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<T> delete(@PathVariable("id") ID id) {
		System.out.println("Fetching & Deleting Produto with id " + id);

		T t = service.findById(id);
		if (t == null) {
			System.out.println("Unable to delete. Produto with id " + id + " not found");
			return new ResponseEntity<T>(HttpStatus.NOT_FOUND);
		}

		service.deleteById(id);
		return new ResponseEntity<T>(HttpStatus.OK);
	}

}
