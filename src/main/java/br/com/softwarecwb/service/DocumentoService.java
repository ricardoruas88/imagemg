package br.com.softwarecwb.service;

import org.springframework.stereotype.Service;

import br.com.softwarecwb.entity.Documento;
import br.com.softwarecwb.repository.DocumentoRepository;
import br.com.softwarecwb.service.generic.AbstractService;

@Service("documentoService")
public class DocumentoService extends AbstractService<Documento, Integer> {

	public DocumentoService(DocumentoRepository repository) {
		super(repository);
		// TODO Auto-generated constructor stub
	}
	
}