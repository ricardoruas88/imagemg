package br.com.softwarecwb.service;

import org.springframework.stereotype.Service;

import br.com.softwarecwb.entity.Empresa;
import br.com.softwarecwb.repository.EmpresaRepository;
import br.com.softwarecwb.service.generic.AbstractService;

@Service("empresaService")
public class EmpresaService extends AbstractService<Empresa, Integer> {

	public EmpresaService(EmpresaRepository repository) {
		super(repository);
		// TODO Auto-generated constructor stub
	}
	
}
