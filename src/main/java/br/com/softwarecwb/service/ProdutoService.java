package br.com.softwarecwb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.softwarecwb.entity.Produto;
import br.com.softwarecwb.repository.ProdutoRepository;
import br.com.softwarecwb.service.generic.AbstractService;

@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@Service("produtoService")
public class ProdutoService extends AbstractService<Produto, Integer> {

	@Autowired
	public ProdutoService(ProdutoRepository repository) {
		super(repository);
	}
}
