package br.com.softwarecwb.service.generic;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class AbstractService<T, ID extends Serializable> implements IAbstractService<T, ID> {

	private JpaRepository<T, ID> repository;

	public AbstractService(JpaRepository<T, ID> repository) {
		this.repository = repository;
	}

	@Override
	public T findById(ID id) {
		return repository.findOne(id);
	}

	@Transactional(readOnly = false)
	@Override
	public void save(T t) {
		repository.save(t);
	}

	@Transactional(readOnly = false)
	@Override
	public void update(T currentT) {
		repository.save(currentT);

	}

	@Transactional(readOnly = false)
	@Override
	public void deleteById(ID id) {
		repository.delete(id);

	}

	@Override
	public List<T> findAll() {
		return repository.findAll();
	}

}
