package br.com.softwarecwb.service.generic;

import java.util.List;

public interface IAbstractService<T, ID> {

	T findById(ID id);

	void save(T t);

	
	void update(T currentT);

	void deleteById(ID id);

	List<T> findAll();
}
