package br.com.softwarecwb.service.util;

import java.io.FileInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class LeitorDeXml<T> {

	@SuppressWarnings("unchecked")
	public T LerXml(FileInputStream file, Class<T> classe) throws JAXBException {
		JAXBContext context = null;

		context = JAXBContext.newInstance(classe);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		return (T) unmarshaller.unmarshal(file);

	}
}
