package br.com.softwarecwb.service.xml;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "channel")
@XmlAccessorType (XmlAccessType.FIELD)
public class ChannelXML {

	
	@XmlElement(name = "item")
	private List<ItemXML> itens = null;

	public List<ItemXML> getItens() {
		return itens;
	}

	public void setItens(List<ItemXML> itens) {
		this.itens = itens;
	}

	
	
}
