package br.com.softwarecwb.service.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class ItemXML {

	private String codigo;

	private String nome;

	private String link;

	private String descricao;

	private String imagemLink;

	private String tipoProduto;

	public String getCodigo() {
		return codigo;
	}

	@XmlElement(name = "id")
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	@XmlElement(name = "title")
	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLink() {
		return link;
	}

	@XmlElement(name = "link")
	public void setLink(String link) {
		this.link = link;
	}

	public String getDescricao() {
		return descricao;
	}

	@XmlElement(name = "description")
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getImagemLink() {
		return imagemLink;
	}

	@XmlElement(name = "image_link")
	public void setImagemLink(String imagemLink) {
		this.imagemLink = imagemLink;
	}

	public String getTipoProduto() {
		return tipoProduto;
	}

	@XmlElement(name = "product_type")
	public void setTipoProduto(String tipoProduto) {
		this.tipoProduto = tipoProduto;
	}

	public ItemXML() {

	}

}
