angular.module('qrcode')
	.controller('DocumentoController', ['$scope', 'actionsDocumentoCRUD', '$routeParams', 'cadastroDeDocumentos',
		function($scope, actionsDocumentoCRUD, $routeParams, cadastroDeDocumentos) {

		$scope.documento = {};
		$scope.mensagem = '';
		
		
		if($routeParams.documentoId) {
			actionsDocumentoCRUD.get({documentoId: $routeParams.documentoId}, function(documentoEdit) {
				$scope.documento.id = documentoEdit.id;
				$scope.documento.documento = documentoEdit.documento;
				$scope.documento.titulo = documentoEdit.titulo;
				$scope.documento.tipo = documentoEdit.tipo;
			}, function(erro) {
				console.log(erro);
				$scope.mensagem = 'Não foi possível obter um produto'
			});
			
		}
		
		$scope.submeter = function() {
			if ($scope.formulario.$valid) {
				cadastroDeDocumentos.cadastrar($scope.documento,$scope.uploadFile)
				.then(function(dados) {
					$scope.mensagem = dados.mensagem;
					$scope.documento = {};
					$scope.uploadFile = {};
					$scope.previewImage = {};
				})
				.catch(function(erro) {
					$scope.mensagem = erro.mensagem;
				});
			}
		};
	}]);