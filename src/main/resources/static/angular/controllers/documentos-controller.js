angular.module('qrcode').controller('DocumentosController', function($scope, actionsDocumentoCRUD,$http) {
	
	$scope.documentos = [];
	$scope.filtro = '';
	$scope.mensagem = '';

	actionsDocumentoCRUD.query(function(documentos) {
		$scope.documentos = documentos;
	}, function(erro) {
		console.log(erro);
	});
	
	$scope.download = function(documento)
	{
		$http.get('/documento/download/'+documento.id, {responseType: "arraybuffer"}).success(function(data){
	         
	    var arrayBufferView = new Uint8Array( data );
	    var blob = new Blob( [ arrayBufferView ], { type: documento.tipo } );
	    var urlCreator = window.URL || window.webkitURL;
	    var fileURL = urlCreator.createObjectURL( blob );
        var fileName = documento.filename;
        
        if (!window.ActiveXObject) {
            var save = document.createElement('a');
            save.href = fileURL;
            save.target = '_blank';
            save.download = fileName || 'unknown';

            var event = document.createEvent('Event');
            event.initEvent('click', true, true);
            save.dispatchEvent(event);
            (window.URL || window.webkitURL).revokeObjectURL(save.href);
        }

        // for IE
        else if ( !! window.ActiveXObject && document.execCommand)     {
            var _window = window.open(fileURL, '_blank');
            _window.document.close();
            _window.document.execCommand('SaveAs', true, fileName || fileURL)
            _window.close();
        }
	        }).error(function(err, status){})
        
	};
	
	
	$scope.remover = function(documento) {

		actionsDocumentoCRUD.delete({documentoId: documento.id}, function() {
			var indiceDoDocumento = $scope.documentos.indexOf(documento);
			$scope.documentos.splice(indiceDoDocumento, 1);
			$scope.mensagem = 'Documento ' + documento.titulo + ' removida com sucesso!';
		}, function(erro) {
			console.log(erro);
			$scope.mensagem = 'Não foi possível apagar a documento ' + documento.titulo;
		});
	};
	
	$scope.editar = function(documento) {
		actionsDocumentoCRUD.get({documentoId: documento.id}, function(documento) {
			$scope.documento = documento;
		}, function(erro) {
			console.log(erro);
			$scope.mensagem = 'Não foi possível obter o documento'
		});
	};
});