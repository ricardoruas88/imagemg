angular.module('qrcode')
	.controller('EmpresaController', ['$scope', 'actionsEmpresaCRUD', '$routeParams', 'cadastroDeEmpresas',
		function($scope, actionsEmpresaCRUD, $routeParams, cadastroDeEmpresas) {

		$scope.empresa = {};
		$scope.mensagem = '';
		
		if($routeParams.empresaId) {
			actionsEmpresaCRUD.get({empresaId: $routeParams.empresaId}, function(empresaEdit) {
				var empresa = null;
				$scope.empresa.id = empresaEdit.id;
				$scope.empresa.logomarca = empresaEdit.logomarca;
				$scope.empresa.nome = empresaEdit.nome;
			}, function(erro) {
				console.log(erro);
				$scope.mensagem = 'Não foi possível obter um produto'
			});
			
		}
		
		$scope.submeter = function() {
			if ($scope.formulario.$valid) {
				cadastroDeEmpresas.cadastrar($scope.empresa,$scope.uploadFile)
				.then(function(dados) {
					$scope.mensagem = dados.mensagem;
					$scope.empresa = {};
					$scope.uploadFile = {};
					$scope.previewImage = {};
				})
				.catch(function(erro) {
					$scope.mensagem = erro.mensagem;
				});
			}
		};
	}]);