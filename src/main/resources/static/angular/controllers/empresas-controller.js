angular.module('qrcode').controller('EmpresasController', function($scope, actionsEmpresaCRUD) {
	
	$scope.empresas = [];
	$scope.filtro = '';
	$scope.mensagem = '';

	actionsEmpresaCRUD.query(function(empresas) {
		$scope.empresas = empresas;
	}, function(erro) {
		console.log(erro);
	});

	$scope.remover = function(empresa) {

		actionsEmpresaCRUD.delete({empresaId: empresa.id}, function() {
			var indiceDoEmpresa = $scope.empresas.indexOf(empresa);
			$scope.empresas.splice(indiceDoEmpresa, 1);
			$scope.mensagem = 'Empresa ' + empresa.nome + ' removida com sucesso!';
		}, function(erro) {
			console.log(erro);
			$scope.mensagem = 'Não foi possível apagar a empresa ' + empresa.nome;
		});
	};
	
	$scope.editar = function(empresa) {
		actionsEmpresaCRUD.get({empresaId: empresa.id}, function(empresa) {
			$scope.empresa = empresa;
		}, function(erro) {
			console.log(erro);
			$scope.mensagem = 'Não foi possível obter o empresa'
		});
	};
});