angular.module('qrcode')
	.controller('ProdutoController', ['$scope', 'actionsProdutoCRUD','actionsEmpresaCRUD', '$routeParams', 'cadastroDeProdutos', 
		function($scope, actionsProdutoCRUD,actionsEmpresaCRUD, $routeParams, cadastroDeProdutos) {

		$scope.produto = {};
		$scope.mensagem = '';
		$scope.selected = {};
		
		actionsEmpresaCRUD.query(function(empresas) {
			$scope.empresas = empresas;
		}, function(erro) {
			console.log(erro);
		});
		
		if($routeParams.produtoId) {
			actionsProdutoCRUD.get({produtoId: $routeParams.produtoId}, function(produto) {
				$scope.produto = produto; 
			}, function(erro) {
				console.log(erro);
				$scope.mensagem = 'Não foi possível obter um produto'
			});
			
		}
		
		$scope.submeter = function() {
			if ($scope.formulario.$valid) {
				angular.forEach($scope.empresas,function(value,key)
				{
					if(value.id == $scope.produto.empresa.id)
						$scope.produto.empresa=value;
				});
				cadastroDeProdutos.cadastrar($scope.produto)
				.then(function(dados) {
					$scope.mensagem = dados.mensagem;
					$scope.produto = {};
				})
				.catch(function(erro) {
					$scope.mensagem = erro.mensagem;
				});
			}
		};
	}]);