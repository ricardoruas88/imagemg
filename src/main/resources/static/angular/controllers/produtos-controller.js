angular.module('qrcode').controller('ProdutosController', function($scope, actionsProdutoCRUD,$http) {
	
	$scope.produtos = [];
	$scope.filtro = '';
	$scope.mensagem = '';

	actionsProdutoCRUD.query(function(produtos) {
		$scope.produtos = produtos;
	}, function(erro) {
		console.log(erro);
	});

	$scope.download = function(produto)
	{
		$http.get('/produto/download/'+produto.id, {responseType: "arraybuffer"}).success(function(data){
	         
	    var arrayBufferView = new Uint8Array( data );
	    var blob = new Blob( [ arrayBufferView ], { type: 'image/png' } );
	    var urlCreator = window.URL || window.webkitURL;
	    var fileURL = urlCreator.createObjectURL( blob );
        var fileName = produto.nome;
        
        if (!window.ActiveXObject) {
            var save = document.createElement('a');
            save.href = fileURL;
            save.target = '_blank';
            save.download = fileName || 'unknown';

            var event = document.createEvent('Event');
            event.initEvent('click', true, true);
            save.dispatchEvent(event);
            (window.URL || window.webkitURL).revokeObjectURL(save.href);
        }

        // for IE
        else if ( !! window.ActiveXObject && document.execCommand)     {
            var _window = window.open(fileURL, '_blank');
            _window.document.close();
            _window.document.execCommand('SaveAs', true, fileName || fileURL)
            _window.close();
        }
	        }).error(function(err, status){})
        
	};
	
	$scope.remover = function(produto) {

		actionsProdutoCRUD.delete({produtoId: produto.id}, function() {
			var indiceDoProduto = $scope.produtos.indexOf(produto);
			$scope.produtos.splice(indiceDoProduto, 1);
			$scope.mensagem = 'Produto ' + produto.nome + ' removida com sucesso!';
		}, function(erro) {
			console.log(erro);
			$scope.mensagem = 'Não foi possível apagar a produto ' + produto.nome;
		});
	};
	
	$scope.editar = function(produto) {
		actionsProdutoCRUD.get({produtoId: produto.id}, function(produto) {
			$scope.produto = produto;
		}, function(erro) {
			console.log(erro);
			$scope.mensagem = 'Não foi possível obter o produto'
		});
	};
});