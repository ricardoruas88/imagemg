angular.module('diretivas', []).directive('headerPrincipal', function() {

	var ddo = {};

	ddo.restrict = "AE";
	ddo.transclude = true;

	ddo.templateUrl = 'templates/diretivas/header-principal.html';

	return ddo;
}).directive('sidebarPrincipal', function() {

	var ddo = {};

	ddo.restrict = "AE";
	ddo.transclude = true;

	ddo.templateUrl = 'templates/diretivas/sidebar-principal.html';

	return ddo;
})

.directive('controlSidebarPrincipal', function() {

	var ddo = {};

	ddo.restrict = "AE";
	ddo.transclude = true;

	ddo.templateUrl = 'templates/diretivas/control-sidebar-principal.html';

	return ddo;
}).directive('footerPrincipal', function() {

	var ddo = {};

	ddo.restrict = "AE";
	ddo.transclude = true;

	ddo.templateUrl = 'templates/diretivas/footer-principal.html';

	return ddo;
}).directive('fileModel', [ '$parse', function($parse) {
	return {
		restrict : 'A',
		link : function(scope, element, attrs) {
			var model = $parse(attrs.fileModel);
			var modelSetter = model.assign;
			element.bind('change', function() {
				scope.$apply(function() {
					modelSetter(scope, element[0].files[0]);
				});
			});
		}
	};
} ]);