angular
		.module('diretivasDocumento', [])
		.directive(
				'tabelaDocumentos',
				function() {

					var ddo = {};

					ddo.restrict = "AE";
					ddo.transclude = true;

					ddo.templateUrl = 'templates/diretivas/documento/tabela-documentos.html';

					return ddo;
				})