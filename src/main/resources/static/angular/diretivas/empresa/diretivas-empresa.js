angular
		.module('diretivasEmpresa', [])
		.directive(
				'tabelaEmpresas',
				function() {

					var ddo = {};

					ddo.restrict = "AE";
					ddo.transclude = true;

					ddo.templateUrl = 'templates/diretivas/empresa/tabela-empresas.html';

					return ddo;
				})
		.directive(
				'logomarca',
				function() {

					var ddo = {};

					ddo.restrict = "AE";

					ddo.scope = {
						url : '@'
					};

					ddo.template = '<img class="img-responsive center-block" src="{{url}}"/>';

					return ddo;
				})