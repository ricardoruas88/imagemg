angular.module('diretivasProduto', []).directive('tabelaProdutos', function() {

	var ddo = {};

	ddo.restrict = "AE";
	ddo.transclude = true;

	ddo.templateUrl = 'templates/diretivas/produto/tabela-produtos.html';

	return ddo;
});