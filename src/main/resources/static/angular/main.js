angular
		.module(
				'qrcode',
				[ 'diretivas', 'diretivasProduto', 'diretivasEmpresa',
						'diretivasDocumento', 'ngRoute', 'ngResource',
						'datatables', 'ProdutoService', 'EmpresaService',
						'DocumentoService', 'bootstrap.fileField' ])
		.config(
				[
						'$routeProvider',
						'$locationProvider',
						'$compileProvider',
						function($routeProvider, $locationProvider,
								$compileProvider) {
							$compileProvider
									.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension|blob|):/);
							
							$routeProvider
									.when(
											'/list',
											{
												templateUrl : 'templates/partial/produtos.html',
												controller : 'ProdutosController'
											})
									.when(
											'/new',
											{
												templateUrl : 'templates/partial/form/form-produto.html',
												controller : 'ProdutoController'
											})
									.when(
											'/edit/:produtoId',
											{
												templateUrl : 'templates/partial/form/form-produto.html',
												controller : 'ProdutoController'
											})
									.when(
											'/empresas',
											{
												templateUrl : 'templates/partial/empresas.html',
												controller : 'EmpresasController'
											})
									.when(
											'/empresas/edit/:empresaId',
											{
												templateUrl : 'templates/partial/form/form-empresa.html',
												controller : 'EmpresaController'
											})
									.when(
											'/empresas/new',
											{
												templateUrl : 'templates/partial/form/form-empresa.html',
												controller : 'EmpresaController'
											})
									.when(
											'/documentos',
											{
												templateUrl : 'templates/partial/documentos.html',
												controller : 'DocumentosController'
											})
									.when(
											'/documentos/edit/:documentoId',
											{
												templateUrl : 'templates/partial/form/form-documento.html',
												controller : 'DocumentoController'
											})
									.when(
											'/documentos/new',
											{
												templateUrl : 'templates/partial/form/form-documento.html',
												controller : 'DocumentoController'
											}).otherwise({
										redirectTo : '/list'
									});

							// $locationProvider.html5Mode({
							// enabled: true,
							// requireBase: true
							// });
						} ]);