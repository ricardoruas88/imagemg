angular
		.module('DocumentoService', [ 'ngResource' ])
		.factory('actionsDocumentoCRUD', function($resource) {

			return $resource('/documento/:documentoId', null, {
				'update' : {
					method : 'PUT'
				},
				'uploadFile' : {
					method : 'POST',
					transformRequest : angular.identity,
					headers : {
						'Content-Type' : undefined
					}
				}
			});
		})
		.factory('DocumentResource', function($scope, $resource) {  
			  $resource('document/:id', { id: "@id" }, {
					    download: {
					      method: 'GET',
					      responseType: 'arraybuffer'
					    }
					  })
					})
		.factory(
				"cadastroDeDocumentos",
				function(actionsDocumentoCRUD, $q, $http) {

					var service = {};

					service.cadastrar = function(documento, file) {
						return $q(function(resolve, reject) {
							if (file != null) {
								var fd = new FormData();
								documento.documento = '';
								documento.tipo = file.type;
								fd.append('documento', angular.toJson(
										documento, true));
								fd.append('file', file);
								// send the file / data to your server
								$http
										.post(
												'/documento/uploadFile',
												fd,
												{
													transformRequest : angular.identity,
													headers : {
														'Content-Type' : undefined
													}
												})
										.success(

												function(data) {
													resolve({
														mensagem : 'Documento '
																+ documento.titulo
																+ ' salvo com sucesso'
													});
												})
										.error(

												function(err) {
													reject({
														mensagem : 'Não foi possível salvar o documento '
																+ documento.titulo
													});
												});

							} else {
								if (documento.id) {
									actionsDocumentoCRUD
											.update(
													{
														documentoId : documento.id
													},
													documento,
													function() {
														resolve({
															mensagem : 'Documento '
																	+ documento.titulo
																	+ ' atualizada com sucesso'
														});
													},
													function(erro) {
														console.log(erro);
														reject({
															mensagem : 'Não foi possível atualizar a documento '
																	+ documento.titulo
														});
													});

								} else {
									actionsDocumentoCRUD
											.save(
													documento,

													function() {
														resolve({
															mensagem : 'Documento '
																	+ documento.titulo
																	+ ' incluída com sucesso',
														});
													},
													function(erro) {
														console.log(erro);
														reject({
															mensagem : 'Não foi possível incluir a documento '
																	+ documento.titulo
														});
													});
								}
							}

						});
					}

					return service;

				});