angular.module('EmpresaService', ['ngResource']).factory('actionsEmpresaCRUD', function ($resource) {

    return $resource('/empresa/:empresaId', null, {
        'update': {
            method: 'PUT'
        },
        'uploadFile': {
            method: 'POST',
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined
            }
        }
    });
}).factory("cadastroDeEmpresas", function (actionsEmpresaCRUD, $q, $http) {

    var service = {};

    service.cadastrar = function (empresa, file) {
        return $q(function (resolve, reject) {
            if (file != null) {
                var fd = new FormData();
                empresa.logomarca = '';
                fd.append('empresa', angular.toJson(empresa, true));
                fd.append('file', file);

                //send the file / data to your server
                $http.post('/empresa/uploadFile', fd, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).success(function (data) {
                    resolve({
                        mensagem: 'Empresa ' + empresa.nome + ' incluída com sucesso',
                    });
                }).error(function (err) {
                    reject({
                        mensagem: 'Não foi possível atualizar a empresa ' + empresa.nome
                    });
                });

            } else {
                if (empresa.id) {
                    actionsEmpresaCRUD.update({
                        empresaId: empresa.id
                    }, empresa, function () {
                        resolve({
                            mensagem: 'Empresa ' + empresa.nome + ' atualizada com sucesso'
                        });
                    }, function (erro) {
                        console.log(erro);
                        reject({
                            mensagem: 'Não foi possível atualizar a empresa ' + empresa.nome
                        });
                    });

                } else {
                    actionsEmpresaCRUD.save(
                    empresa,

                    function () {
                        resolve({
                            mensagem: 'Empresa ' + empresa.nome + ' incluída com sucesso',
                        });
                    }, function (erro) {
                        console.log(erro);
                        reject({
                            mensagem: 'Não foi possível incluir a empresa ' + empresa.nome
                        });
                    });
                }
            }

        });
    }

    return service;

});