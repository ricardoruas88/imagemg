    angular.module('ProdutoService', ['ngResource'])
    .factory('actionsProdutoCRUD', function($resource) {

        return $resource('/produto/:produtoId', null, {
            'update' : { 
                method: 'PUT'
            }
        });
    })
    
    .factory("cadastroDeProdutos", function(actionsProdutoCRUD, $q) {

        var service = {};

        service.cadastrar = function(produto) {
            return $q(function(resolve, reject) {
            	
                if(produto.id) {
                	actionsProdutoCRUD.update({produtoId: produto.id}, produto, function() {
                        resolve({
                            mensagem: 'Produto ' + produto.nome + ' atualizada com sucesso'
                        });
                    }, function(erro) {
                        console.log(erro);
                        reject({
                            mensagem: 'Não foi possível atualizar a produto ' + produto.nome
                        });
                    });

                } else {
                	actionsProdutoCRUD
					.save(
							produto,
							function() {
								resolve({
									mensagem : 'Produto '
											+ produto.nome
											+ ' incluída com sucesso',
								});
							},
							function(erro) {
								console.log(erro);
								reject({
									mensagem : 'Não foi possível incluir a produto '
											+ produto.nome
								});
							});
                }
            });
        };
        return service;
    });